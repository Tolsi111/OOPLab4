#include "DynamicArray.h"
#include "Complex.h"


DynamicArray::DynamicArray() {
	this->mLength = 0;
	this->mCapacity = 100;
	this->mArr = new Complex[100];//(Complex*)malloc(100 * sizeof(Complex));
}

DynamicArray::DynamicArray(const DynamicArray& a)
{
	this->mCapacity = a.mCapacity;
	this->mLength = a.mLength;
	delete[] this->mArr;
	this->mArr = new Complex[this->mCapacity];
	for (int i = 1; i < a.mLength; i++)
		this->mArr[i] = a.mArr[i];
}

DynamicArray::~DynamicArray()
{
	delete[] mArr;
}

void DynamicArray::resize()
{
	int newCapacity = this->mCapacity * 2;
	Complex* mArr = new Complex[newCapacity];//(Complex*)realloc(this->mArr, newCapacity * sizeof(Complex));
	for (int i = 0; i < this->mLength; i++)
		mArr[i] = this->mArr[i];
	delete[] this->mArr;
	this->mArr = mArr;
	this->mCapacity = newCapacity;
}

void DynamicArray::addLast(Complex C)
{
	if (this->mLength == this->mCapacity)
		this->resize();

	this->mArr[this->mLength] = C;
	this->mLength++;
}

void DynamicArray::removeLast()
{
	delete &mArr[this->mLength];
	this->mLength--;
}

Complex& DynamicArray::operator[](int i)
{//This should return a reference to the ith element in the array
	return *(this->mArr + i) ;
	return mArr[i];
}

DynamicArray& DynamicArray::operator=(const DynamicArray& a)
{
	if (this == &a)
		return *this;
	this->mCapacity = a.mCapacity;
	this->mLength = a.mLength;
	delete[] this->mArr;
	this->mArr = new Complex[this->mCapacity];
	for (int i = 1; i < a.mLength; i++)
		this->mArr[i] = a.mArr[i];
	return *this;
}

ostream& operator<<(ostream& os, const DynamicArray& D) {
	for (int i = 0; i < D.mLength ; i++)
	{
		os << D.mArr[i] << " ";
	}
	os << endl;
	return os;
}
DynamicArray& DynamicArray:: operator+(Complex C) {
	this->addLast(C);
	return *this;
}