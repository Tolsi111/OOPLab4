#include "Complex.h"

Complex::Complex() {
	this->mReal = 0;
	this->mImag = 0;
}

Complex::Complex(float real, float imaginar) {
	this->mReal = real;
	this->mImag = imaginar;
}

Complex Complex::add(Complex C)
{
	Complex toReturn;

	toReturn.mReal = this->mReal + C.mReal;
	toReturn.mImag = this->mImag + C.mImag;

	return toReturn;
}

Complex Complex::operator+(Complex C) {
	return this->add(C);
}

Complex Complex::operator-(Complex C)
{
	Complex toReturn;

	toReturn.mReal = this->mReal - C.mReal;
	toReturn.mImag = this->mImag - C.mImag;

	return toReturn;

}

Complex Complex::operator*(Complex C)
{
	Complex toReturn;

	toReturn.mReal = (this->mReal * C.mReal) - (this->mImag * C.mImag);
	toReturn.mImag = (this->mReal * C.mImag) + (this->mImag * C.mReal);

	return toReturn;
}

Complex Complex::operator/(Complex C)
{
	Complex toReturn;

	float denominator = (C.mReal * C.mReal) + (C.mImag * C.mImag);

	C.mImag = C.mImag * -1;
	toReturn.mReal = (this->mReal * C.mReal) - (this->mImag * C.mImag);
	toReturn.mImag = (this->mReal * C.mImag) + (this->mImag * C.mReal);

	toReturn.mReal = toReturn.mReal / denominator;
	toReturn.mImag = toReturn.mImag / denominator;


	return toReturn;
}

bool operator==(const Complex& C1, const Complex& C2)
{
	return (C1.mReal == C2.mReal) && (C1.mImag == C2.mImag);
}

bool operator!=(const Complex& C1, const Complex& C2)
{
	return (C1.mReal != C2.mReal) || (C1.mImag != C2.mImag);
}

ostream& operator<<(ostream& os, const Complex& C) {
	os << C.mReal << "+" << C.mImag << "i" << endl;
	return os;
}

istream& operator>>(istream& is, Complex& C) {
	is >> C.mReal >> C.mImag;
	return is;
}