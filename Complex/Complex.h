#pragma once
#include <istream>
#include <ostream>

using namespace std;

class Complex {
public:
	//Default constructor and a constructor that initializes a complex number with the real and imaginary parts.
	Complex();
	Complex(float real, float imaginar);

	Complex add(Complex);

	//Overloads for the equal to and not equal to operators.
	friend bool operator==(const Complex& C1, const Complex& C2);
	friend bool operator!=(const Complex& C1, const Complex& C2);

	//Overload for the stream insertion operator (>>) and stream extraction (<<) operators.
	friend ostream& operator<<(ostream& os, const Complex&);//functie friend
	friend istream& operator>>(istream& is, Complex&);

	//Overloads for the arithmetic operators: addition (+), subtraction (-), multiplication(*) and division(/).
	Complex operator+(Complex);
	Complex operator-(Complex);
	Complex operator*(Complex);
	Complex operator/(Complex);

	float getReal() const { return mReal; }
	float getImag() const { return mImag; }

private:
	float mReal, mImag;
};