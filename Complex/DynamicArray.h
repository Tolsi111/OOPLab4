#pragma once
#include "Complex.h"

#include <istream>
#include <ostream>
#include<vector>
using namespace std;

class DynamicArray
{
	public:
		DynamicArray();//Default constructor
		DynamicArray(const DynamicArray& a);//Copy constructor		#2
		~DynamicArray();//Destructor		#1
		void addLast(Complex);//Insert at element at the end of the array
		void removeLast();//Remove the last element of the array
		int getLen() const { return mLength; }//Getter for the length of the array
		int getCap() const { return mCapacity; }//Getter for the capacity
		friend ostream& operator<<(ostream& os, const DynamicArray&);//Overload for the stream insertion operator (<<)
		Complex& operator[](int i);//Overload for the indexing/subscript operator ([])

		DynamicArray& operator=(const DynamicArray&);//user-defined copyassignment operator		#3
		void resize();
		DynamicArray& operator+(Complex);//adds an element to the DynamicArray

	private:
		int mLength, mCapacity;
		Complex* mArr;
};

