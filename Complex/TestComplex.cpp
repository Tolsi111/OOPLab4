#include "TestComplex.h"
#include "Complex.h"
#include <assert.h>
#include <iostream>

using namespace std;

void testAllComplex() {

	//Default constructor and a constructor that initializes a complex number with the real and imaginary parts.
	Complex C1;
	assert(C1.getReal() == 0 && C1.getImag() == 0);
	Complex C2(1, 2);
	assert(C2.getReal() == 1 && C2.getImag() == 2);

	//Overloads for the equal to and not equal to operators.
	Complex C3(1, 2);
	assert(C1 != C2);
	assert(C1 != C3);
	assert(C2 == C3);

	//Overload for the stream insertion operator (>>) and stream extraction (<<) operators.
	//Complex C4;
	//cout << "Please input the real then the imaginary part of a complex number" << endl;
	//cin >> C4;
	//cout << "Your Complex number is : " << C4 << endl;

	//Overloads for the arithmetic operators: addition (+), subtraction (-), multiplication(*) and division(/).
	assert(C1 + C2 == C3);//adition

	Complex ans1(-1, -2);//subtraction
	assert(C1 - C2 == ans1);

	Complex C5(-1, -5), C6(10, 12), ans2(50, -62);// (-1 - 5i)*(10 + 12i)
	assert(C5 * C6 == ans2);

	Complex C7(20, -4), C8(3, 2), ans3(4,-4);//division
	assert(C7 / C8 == ans3);
}