#include "pch.h"
#include "CppUnitTest.h"
#include "../Complex/Complex.h"
#include "../Complex/DynamicArray.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		/*TEST_METHOD(CopyConstructor)
		{
			Complex e(10, 2);
			DynamicArray arr1;
			arr1.addLast(e);
			DynamicArray arr2 = arr1;
			Assert::AreEqual(arr1,arr2);
		}*/
		TEST_METHOD(addLast)
		{
			DynamicArray arr;
			Complex x;
			arr.addLast(x);
			Assert::AreEqual(arr[0], x);
		}
	};
}
